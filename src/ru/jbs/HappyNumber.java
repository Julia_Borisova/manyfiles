package ru.jbs;

import java.io.*;

/**
 * Класс, который на основе файла int6data.dat формиромирует текстовый файл intHappy6data.dat, содержащий только
 счастливые номера.
 */
public class HappyNumber {
    public static void main(String[] args) throws IOException {

        try (DataOutputStream output = new DataOutputStream(new FileOutputStream("intHappy6Data.dat"))) {
            try (DataInputStream input = new DataInputStream(new FileInputStream("int6Data.dat"))) {
                int number;

                while (input.available() != 0) {
                    number = input.readInt();
                    if (SearchForLuckyNumbers(number) == true) {
                        output.writeInt(number);
                    }

                }
            }

        } catch (FileNotFoundException ex) {
            System.err.println("Не удается найти указанный файл.");
            System.exit(0);
        }
    }

    /**
     * Метод определяет является ли число счастливым.
     * @param number
     * @return значение true, если счастливый ,если нет - false.
     */
    private static boolean SearchForLuckyNumbers(int number) {
        int FirstNumbers;
        int SecondNumbers;
        double SumFirstNumbers;
        double SumSecondNumbers;
        FirstNumbers=number/1000;
        SecondNumbers=number%1000;
        SumFirstNumbers=(FirstNumbers/100) +(FirstNumbers%10) + (FirstNumbers%100/10);
        SumSecondNumbers=(SecondNumbers/100) +(SecondNumbers%10) + (SecondNumbers%100/10);
        if (SumFirstNumbers==SumSecondNumbers) {
            return true;
        }
        else {
            return false;
        }

    }
}