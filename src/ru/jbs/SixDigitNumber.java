package ru.jbs;

import java.io.*;

/**
 * Класс, который на основе данных файла intdata.dat формирует файл int6data.dat,
 * содержащий только шестизначные целые числа.
 */
public class SixDigitNumber {
    public static void main(String[] args) throws IOException {

        try (DataOutputStream output = new DataOutputStream(new FileOutputStream("int6Data.dat"))) {
            try (DataInputStream input = new DataInputStream(new FileInputStream("intData.dat"))) {
                int number;
                while (input.available() > 3) {
                    number = input.readInt();
                    if (number > 99999 && number < 1000000) {
                        output.writeInt(number);
                    }
                }
            } catch (FileNotFoundException ex) {
                System.err.println("Не удается найти указанный файл.");
                System.exit(0);
            }
        }

    }
}

