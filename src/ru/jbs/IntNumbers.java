package ru.jbs;

import java.io.*;

/**
 * Класс, в котором числа из файла intNumber.txt  записываются в файл intdata.dat
 */
public class IntNumbers {
    public static void main(String[] args) throws IOException {

        try (DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream("intData.dat"))) {
            try (BufferedReader reader = new BufferedReader(new FileReader("intNumber.txt"))) {
                String number;
                while ((number = reader.readLine()) != null) {
                    try {
                        dataOutputStream.writeInt(Integer.parseInt(number));

                    } catch (NumberFormatException ex) {
                        System.err.println("Встречены некорректные данные.");
                    }
                }

            } catch (IOException ex) {
                System.err.println("Не удается найти указанный файл.");
                System.exit(0);
            }
        }
    }
}